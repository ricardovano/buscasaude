app.controller('listaUnidadesCtl', function ($scope, $http) {
    $scope.name = gup("pesquisa").replace(/%20/g, ' ');
    $scope.loading = true;

    $http.get(getUrl('Saude') + '/GetUnidadeByName?name=' + $scope.name )
    .success(function(data) {
        $scope.unidades = data;
        $scope.loading = false;
    })
    .error(function(data) {
        console.log('Erro: ' + data);
        $scope.loading = false;
    });

    $scope.getDetail = function(id, name){
        var nameToUrl = name.replace(/ /g, '-');
        location.href = "/Unidade/centro-medico.html?" + nameToUrl + "&id="+id;
    }

    $scope.getColor = function(index){
        if(index % 2 == 0)
            return '#f3f3f3';

        return '#fff';
    }


});