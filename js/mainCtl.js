app.controller('mainCtl', function ($scope, $http, $cookies) {
    $scope.tipo = -1;
    $scope.name = "";
    $scope.city = "";
    $scope.message = "";

    $scope.getUnidadeName = function(){
        $scope.message = "";
        if ($scope.name.length > 1){
            $scope.name = removeAccents($scope.name);
            location.href = "/Unidade/unidades-de-saude.html?pesquisa="+$scope.name;
        }  else {
            $scope.message = "Insira o nome do centro médico que deseja!";
        }
    }

    $scope.getUnidadeCity = function(){
        $scope.message = "";
        if ($scope.city.length > 1){
            $cookies.cidade =  $scope.city;
            $scope.city = removeAccents($scope.city);
            var nomeCidade =  $scope.city.replace(' ', '-').replace(' ', '-').replace(' ', '-').replace(' ', '-');
            location.href = "/Cidade/?"+ nomeCidade;
        } else {
            $scope.message = "Insira o nome da cidade que deseja pesquisar!";
        }
    }


    $scope.getNames = function(){
        if ($scope.name.length > 2){
            $http.get(getUrl('Saude') + '/GetUnidadeByKey?nameKey=' + $scope.name )
                .success(function(data) {
                    $scope.miniUnidades = data;
                    $scope.loading = false;
                })
                .error(function(data) {
                    console.log('Erro: ' + data);
                    $scope.loading = false;
                });
        }
    }

    /*var getAvaliacoes = function(){
        $http.get(getUrl('Saude') + '/GetLastAvaliacoes')
            .success(function(data) {
                $scope.unidadesAvaliadas = data;
                $scope.loading = false;
            })
            .error(function(data) {
                console.log('Erro: ' + data);
                $scope.loading = false;
            });
    }
    getAvaliacoes();*/
});