app.controller('unidadeCtl', function ($scope, $http) {
    $scope.avaliacao = {UnidadeId: "", Dados: {}};
    $scope.message = "";
    $scope.agradece = "";
    $scope.loading = true;
    $scope.sent = false;
    var id = gup("id");

    $scope.notasHL = {
        e: "/img/blank-star.png",
        d: "/img/blank-star.png",
        c: "/img/blank-star.png",
        b: "/img/blank-star.png",
        a: "/img/blank-star.png"
    }
    $scope.notasAT = {
        e: "/img/blank-star.png",
        d: "/img/blank-star.png",
        c: "/img/blank-star.png",
        b: "/img/blank-star.png",
        a: "/img/blank-star.png"
    }
    $scope.notasES = {
        e: "/img/blank-star.png",
        d: "/img/blank-star.png",
        c: "/img/blank-star.png",
        b: "/img/blank-star.png",
        a: "/img/blank-star.png"
    }
    $scope.notasOG = {
        e: "/img/blank-star.png",
        d: "/img/blank-star.png",
        c: "/img/blank-star.png",
        b: "/img/blank-star.png",
        a: "/img/blank-star.png"
    }
    $scope.setHLRate = function(valor){
        if (valor == 20){
            $scope.notasHL = {
                e: "/img/star.png",
                d: "/img/blank-star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaHigieneLimpeza = 20;
        }
        if (valor == 40){
            $scope.notasHL = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaHigieneLimpeza = 40;
        }
        if (valor == 60){
            $scope.notasHL = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaHigieneLimpeza = 60;
        }
        if (valor == 80){
            $scope.notasHL = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaHigieneLimpeza = 80;
        }
        if (valor == 100){
            $scope.notasHL = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/star.png"
            }
            $scope.avaliacao.Dados.NotaHigieneLimpeza = 100;
        }
    }
    $scope.setATRate = function(valor){
        if (valor == 20){
            $scope.notasAT = {
                e: "/img/star.png",
                d: "/img/blank-star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaAtendimento = 20;
        }
        if (valor == 40){
            $scope.notasAT = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaAtendimento = 40;
        }
        if (valor == 60){
            $scope.notasAT = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaAtendimento = 60;
        }
        if (valor == 80){
            $scope.notasAT = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaAtendimento = 80;
        }
        if (valor == 100){
            $scope.notasAT = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/star.png"
            }
            $scope.avaliacao.Dados.NotaAtendimento = 100;
        }
    }
    $scope.setESRate = function(valor){
        if (valor == 20){
            $scope.notasES = {
                e: "/img/star.png",
                d: "/img/blank-star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaEstrutura = 20;
        }
        if (valor == 40){
            $scope.notasES = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaEstrutura = 40;
        }
        if (valor == 60){
            $scope.notasES = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaEstrutura = 60;
        }
        if (valor == 80){
            $scope.notasES = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaEstrutura = 80;
        }
        if (valor == 100){
            $scope.notasES = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/star.png"
            }
            $scope.avaliacao.Dados.NotaEstrutura = 100;
        }
    }
    $scope.setOGRate = function(valor){
        if (valor == 20){
            $scope.notasOG = {
                e: "/img/star.png",
                d: "/img/blank-star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaRecomenda = 20;
        }
        if (valor == 40){
            $scope.notasOG = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/blank-star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaRecomenda = 40;
        }
        if (valor == 60){
            $scope.notasOG = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/blank-star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaRecomenda = 60;
        }
        if (valor == 80){
            $scope.notasOG = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/blank-star.png"
            }
            $scope.avaliacao.Dados.NotaRecomenda = 80;
        }
        if (valor == 100){
            $scope.notasOG = {
                e: "/img/star.png",
                d: "/img/star.png",
                c: "/img/star.png",
                b: "/img/star.png",
                a: "/img/star.png"
            }
            $scope.avaliacao.Dados.NotaRecomenda = 100;
        }
    }

    var refreshEstatistica = function(){
        if ($scope.u.Id){
            $http.get(getUrl('Saude') + '/GetEstatisticaByUnidadeId?estatisticaUnidadeId=' + $scope.u.Id)
                .success(function(estatisticaData) {
                    $scope.pontuacao = estatisticaData;
                })
                .error(function(data) {
                    console.log('Erro: ' + estatisticaData);
                });
        }
    }
    var refresh = function(){
        if (id){
            $http.get(getUrl('Saude') + '/GetUnidadeById?unidadeId=' + id )
                .success(function(data) {
                    $scope.u = data;
                    refreshEstatistica();
                    $scope.avaliacao.UnidadeId = $scope.u.Id;
                    $scope.avaliacao.Dados.Email = "";
                    $scope.avaliacao.Dados.Name = "";
                    $scope.avaliacao.Dados.Descricao = "";
                    $scope.avaliacao.Dados.NotaRecomenda = 0;
                    $scope.avaliacao.Dados.NotaAtendimento = 0;
                    $scope.avaliacao.Dados.NotaHigieneLimpeza = 0;
                    $scope.avaliacao.Dados.NotaEstrutura = 0;
                    $scope.avaliacao.Dados.Data = null;
                    $scope.loading = false;
                })
                .error(function(data) {
                    $scope.loading = false;
                    console.log('Erro: ' + data);
                });
        }
    }
    refresh();

    $scope.sendAvaliacao = function(){
        if (id){

            if ($scope.avaliacao.Dados.Email.length <= 0){
                $scope.message = "Ops! Insira seu email.";
                return;
            }

            if ($scope.avaliacao.Dados.Email.indexOf('@') < 1 || $scope.avaliacao.Dados.Email.indexOf('.') < 1){
                $scope.message = "Xii! Esse email que você inseriu não existe!";
                return;
            }
            if(!$scope.avaliacao.Dados.Anonimo){
                if ($scope.avaliacao.Dados.Name.length <= 0){
                    $scope.message = "Ops! Diga-nos qual é o seu nome.";
                    return;
                }
            }

            if ($scope.avaliacao.Dados.Descricao.length <= 0){
                $scope.message = "Ops! Insira um comentário. Conte pra gente sua experiência.";
                return;
            }

            $scope.message = "";
            $http({
                dataType: "json",
                method: 'POST',
                url: getUrl('Saude') + '/SetAvaliacaoByUnidadeId',
                data: $scope.avaliacao,
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }
            })
                .success(function (data) {
                    $scope.sent = true;
                    $scope.agradece = $scope.avaliacao.Dados.Name;
                    refresh();
                })
                .error(function (data) {
                    console.log('Erro: ' + data);
                });

        }
    }

});
