app.controller('navCtl', function ($scope) {
    $scope.searchNav = "";
    $scope.getSearch = function () {
        if ($scope.searchNav.length > 1) {
            $scope.searchNav = removeAccents($scope.searchNav);
            location.href = "/Unidade/unidades-de-saude.html?pesquisa=" + $scope.searchNav;
        }
    }
});