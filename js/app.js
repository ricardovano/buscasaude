var gup = function (name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)    return "";
    else    return results[1];
}

function removeAccents(value) {
    return value
        .replace(/á/g, 'a')
        .replace(/é/g, 'e')
        .replace(/í/g, 'i')
        .replace(/ó/g, 'o')
        .replace(/ú/g, 'u')
        .replace(/ç/g, 'c')
        .replace(/ü/g, 'u')
        .replace(/ã/g, 'a')
        .replace(/â/g, 'a')
        .replace(/õ/g, 'o')
        .replace(/Á/g, 'A')
        .replace(/É/g, 'E')
        .replace(/Í/g, 'I')
        .replace(/Ó/g, 'O')
        .replace(/Ú/g, 'U')
        .replace(/Ç/g, 'C')
        .replace(/Ü/g, 'U')
        .replace(/Â/g, 'A')
        .replace(/Ã/g, 'A')
        .replace(/Õ/g, 'O');
}

var getUrl = function(controller){
    var testApiURL = 'http://localhost:8389/api/'+controller;
    var productionApiURL = 'http://www.saudeadvisor.com/api/api/'+controller;
    var currentURL = productionApiURL;
    var server = window.location.hostname;
    if (server == 'localhost')
        currentURL = testApiURL;
    else
        currentURL = productionApiURL;

    return currentURL;
}

var app = angular.module('buscaSaude',['ngRoute', 'ngCookies']).
    config([ '$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
}]);

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});
