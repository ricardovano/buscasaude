app.controller('cadastroCtl', function ($scope, $http, $cookies) {
    $scope.messageContact = "";
    $scope.sentContact = false;
    $scope.Contact = {Name: '', Email: '', Descricao:''};
    $scope.sendContact = function(){
        if ($scope.Contact.Email.length <= 0){
            $scope.messageContact = "Por favor insira seu email.";
            return;
        }

        if ($scope.Contact.Email.indexOf('@') < 1 || $scope.Contact.Email.indexOf('.') < 1){
            $scope.messageContact = "Por favor corrija seu email.";
            return;
        }

        if ($scope.Contact.Name.length <= 0){
            $scope.messageContact = "Por favor insira um nome para contato.";
            return;
        }

        $scope.messageContact = "";
        $http({
            dataType: "json",
            method: 'POST',
            url: getUrl('Site') + '/SendContact',
            data: $scope.Contact,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
            .success(function (data) {
                $scope.sentContact = true;
                $scope.agradeceContact = $scope.Contact.Name;
            })
            .error(function (data) {
                console.log('Erro: ' + data);
            });
    }
});